var mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
	
	/* Gender */
	gender: String,
	
	/* Age Division */
	age: String,
	
	/* Dance */
	dance: String
	
});

mongoose.model('Category', CategorySchema);