var mongoose = require('mongoose');

var ContestantSchema = new mongoose.Schema({
	
	/* Contestant Information */
	last_name: String,
	first_name: String,
	birthday: String,
	tribes: String,
	address: String,
	city: String,
	state: String,
	zip_code: String,
	phone: String,
	ssn: String,
	email: String,	
	id: {type: Number, unique: true},
	
	/* Contestant Score Data */
	grand_entry_1: {type: Boolean, default: false},
	grand_entry_2: {type: Boolean, default: false},
	grand_entry_3: {type: Boolean, default: false},
	grand_entry_4: {type: Boolean, default: false},
	events: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Event'}]
	
});

ContestantSchema.methods.updateGrandEntry = function (grandEntryRound, callBack) {
	if(grandEntryRound === '1') {
		if(this.grand_entry_1) {
			this.grand_entry_1 = false;
		} else {
			this.grand_entry_1 = true;
		}
	} else if(grandEntryRound === '2') {
		if(this.grand_entry_2) {
			this.grand_entry_2 = false;
		} else {
			this.grand_entry_2 = true;
		}
	} else if(grandEntryRound == '3') {
		if(this.grand_entry_3) {
			this.grand_entry_3 = false;
		} else {
			this.grand_entry_3 = true;
		}
	} else if(grandEntryRound == '4') {
		if(this.grand_entry_4) {
			this.grand_entry_4 = false;
		} else {
			this.grand_entry_4 = true;
		}
	} else {
		throw new Error("No Grand Entry Score updated");
	}
	
	this.save(callBack);
};

mongoose.model('Contestant', ContestantSchema);