var mongoose = require('mongoose');

var EventSchema = new mongoose.Schema({
	category: String,
	roundOneScore: { type: Number, default: 0 },
	roundTwoScore: { type: Number, default: 0 },
	roundThreeScore: { type: Number, default: 0 },
	roundFourScore: { type: Number, default: 0 },
	extraPoints: { type: Number, default: 0 },
	contestant: {type: mongoose.Schema.Types.ObjectId, ref: 'Contestant'}
});

EventSchema.methods.score = function(optionNumber, score, cb) {

	if(optionNumber == '1') {
		this.roundOneScore = score;
	}
	else if(optionNumber == '2') {
		this.roundTwoScore = score;
	}
	else if(optionNumber == '3') {
		this.roundThreeScore = score;
	}
	else if(optionNumber == '4') {
		this.roundFourScore = score;
	}
	else if(optionNumber == '5') {
		this.extraPoints = score;
	}
	else {
		// do nothing
	}
	
	this.save(cb);
};

EventSchema.methods.scoreZero = function(cb) {
	this.points += 0;
	this.save(cb);
};

mongoose.model('Event', EventSchema);
