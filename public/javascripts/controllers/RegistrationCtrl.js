MainModule.controller('RegCtrl', [
'$scope',
'contestants',
function($scope, contestants){

	/* Intialize Page
	 * 1) Load categories from server
	*/
	var init = function () {
		var formatCategories = [];
		for(var i = 0; i < contestants.categories.length; i++) {
			formatCategories.push({
				value: contestants.categories[i],
				checked: false
			});
		}
		$scope.categories = formatCategories;
	};	
	init();
	
	$scope.registerContestant = function() {
		
		var events = [];
		for(var i = 0; i < $scope.categories.length; i++) {
			var current = $scope.categories[i];
			if(current.checked) {
				events.push(current.value);
			}
		}
		if(events.length < 1 || 
			$scope.last_name === '' || $scope.last_name === undefined ||
			$scope.first_name === '' || $scope.first_name === undefined ||
			$scope.birthday === '' || $scope.birthday === undefined ||
			$scope.tribes === '' || $scope.tribes === undefined ||
			$scope.address === '' || $scope.address === undefined ||
			$scope.city === '' || $scope.city === undefined ||
			$scope.state === '' || $scope.state === undefined ||
			$scope.zipCode === '' || $scope.zipCode === undefined ||
			$scope.phone === '' || $scope.phone === undefined ||
			//$scope.ssn === '' || $scope.ssn === undefined || // verify and secure
			$scope.id === '' || $scope.id === undefined
			) {
				alert("Fill out all information");
				return;
		} else {
			
			if($scope.email === '' || $scope.email === undefined) {
				$scope.email = 'N/A';
			}
			
			contestants.create({
				last_name: $scope.last_name,
				first_name: $scope.first_name,
				birthday: $scope.birthday,
				tribes: $scope.tribes,
				address: $scope.address,
				city: $scope.city,
				state: $scope.state,
				zip: $scope.zipCode,
				phone: $scope.phone,
				ssn: $scope.ssn,
				email: $scope.email,
				id: $scope.id
			}, events);
			
			$scope.last_name = '';
			$scope.first_name = '';
			$scope.birthday = '';
			$scope.tribes = '';
			$scope.address = '';
			$scope.city = '';
			$scope.state = '';
			$scope.zipCode = '';
			$scope.phone = '';
			$scope.ssn = '';
			$scope.email = '';
			$scope.id = '';	
			for(var i = 0; i < $scope.categories.length; i++) {
				$scope.categories[i].checked = false;
			}
		}
	};
}])
