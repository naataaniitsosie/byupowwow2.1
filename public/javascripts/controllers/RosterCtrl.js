MainModule.controller('RosterCtrl', [
'$scope',
'contestants',
function($scope, contestants){

	/* Two-Way bind contestants to view ($scope.contestants) and model (contestants.contestants) */
	$scope.contestants = contestants.contestants;
	
	$scope.removeContestant = function(contestant) {
		var remove = window.confirm("Are you sure you want to DELETE " + contestant.id + "?");
		if(!remove) {
			return;
		}
		var events = contestant.events;
		contestants.remove(contestant);
		
		for(var i = 0; i < events.length; i++) {
			contestants.removeEvent(events[i]);
		}
	};
}])