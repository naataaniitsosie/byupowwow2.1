MainModule.controller('ScoreCtrl', [
'$scope',
'contestants',
function($scope, contestants){

	$scope.GRAND_ENTRY_POINTS = 3;
	
	$scope.contestants = contestants.contestants;
	$scope.categories = contestants.categories;
	$scope.mens = [];
	$scope.womens = [];
	$scope.other = [];
	$scope.total = 87;
	
	
	var init = function() {
		
		/* Sort Categories */
		for(var i = 0; i < $scope.categories.length; i++) {
			var category = $scope.categories[i];
			if(category.search("Men's") != -1 ||
				category.search("Teen Boys'") != -1 ||
				category.search("Junior Boys'") != -1) {
				$scope.mens.push(category);
			} else if(category.search("Women's") != -1 ||
						category.search("Teen Girls'") != -1 ||
						category.search("Junior Girls'") != -1) {
				$scope.womens.push(category);
			} else {
				$scope.other.push(category);
			}
		}
		
		/* Set Default Category */
		$scope.category = "Select Category";
	};
	init();
	
	$scope.getContestants = function(newCategory) {
		$scope.category = newCategory;
		contestants.getByCategory($scope.category);
	};
	
	$scope.setPrevValue = function(value) {
		$scope.prevValue = value;
	};
	
	$scope.updateScore = function(contestant, option) {
		switch(option) {
			case 1:
				if(isNaN(contestant.event.roundOneScore) || contestant.event.roundOneScore == '') {
					contestant.event.roundOneScore = $scope.prevValue;
				}
				else {
					contestants.updateScore(contestant, option, contestant.event.roundOneScore);
				}
				break;
			case 2:
				if(isNaN(contestant.event.roundTwoScore) || contestant.event.roundTwoScore == '') {
					contestant.event.roundTwoScore = $scope.prevValue;
				}
				else {
					contestants.updateScore(contestant, option, contestant.event.roundTwoScore);
				}
				break;	
			case 3:
				if(isNaN(contestant.event.roundThreeScore) || contestant.event.roundThreeScore == '') {
					contestant.event.roundThreeScore = $scope.prevValue;
				}
				else {
					contestants.updateScore(contestant, option, contestant.event.roundThreeScore);
				}
				break;
			case 4:
				if(isNaN(contestant.event.roundFourScore) || contestant.event.roundFourScore == '') {
					contestant.event.roundFourScore = $scope.prevValue;
				}
				else {
					contestants.updateScore(contestant, option, contestant.event.roundFourScore);
				}
				break;
			case 5:
				if(isNaN(contestant.event.extraPoints) || contestant.event.extraPoints == '') {
					contestant.event.extraPoints = $scope.prevValue;
				}
				else {
					contestants.updateScore(contestant, option, contestant.event.extraPoints);
				}
				break;
			default:
				break;
		}
	};
	
}])