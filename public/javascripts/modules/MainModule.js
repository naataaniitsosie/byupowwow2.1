var MainModule = angular.module('powwow', ['ui.router']);

MainModule
.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

	$stateProvider
		.state('roster', {
			url: '/roster',
			templateUrl: '/roster.html',
			controller: 'RosterCtrl',
			resolve: {
				contestantsPromise: ['contestants', function(contestants){
					return contestants.getAll();
				}]
			}
		})
		.state('score', {
			url: '/score',
			templateUrl: '/score.html',
			controller: 'ScoreCtrl',
			resolve: {
				getNoContestants: ['contestants', function(contestants){
					return contestants.getByCategory('get_no_contestants');
				}],
				getCategories: ['contestants', function(contestants) {
					return contestants.getCategories();
				}]
			}
		})
		.state('contestants', {
			url: '/contestants/{id}',
			templateUrl: '/contestants.html',
			controller: 'ContestantsCtrl',
			resolve: {
				contestant: ['$stateParams', 'contestants', function($stateParams, contestants) {
					return contestants.getContestantPromise($stateParams.id);
				}]
			}
		})
		.state('registration', {
			url: '/registration',
			templateUrl: '/registration.html',
			controller: 'RegCtrl',
			resolve: {
				categories: ['contestants', function(contestants) {
					return contestants.getCategories();
				}]
			}
		})
		.state('grandentry', {
			url: '/grandentry',
			templateUrl: '/grandentry.html',
			controller: 'GrandEntryCtrl',
			resolve: {
				getAllContestants: ['contestants', function(contestants){
					return contestants.getAll();
				}]
			}
		});

	$urlRouterProvider.otherwise('registration');
}])
;


