MainModule.factory('contestants', ['$http', function($http) {
	var o = {
		contestants:  [],
		categories: []
		};
	
	o.clear = function() {
		contestants = [];
	};
	
	o.getContestantPromise = function(id){
		return $http.get('/contestants/' + id).then(function(res){
			
			var newContestant = res.data;
			var newEvents = [];
			for(var i = 0; i < newContestant.events.length; i++) {
				o.getEvent(newContestant.events[i], newEvents);
			}
			
			return {contestant: newContestant, events: newEvents};
		});
	};
	
	o.getEvent = function(id, events) {
		return $http.get('/events/' + id).then(function(res){
			events.push(res.data);
		});
	};
	
	o.getContestantWithEvent = function(event){
		return $http.get('/contestants/' + event.contestant).then(function(res){
			var contestant = res.data;
			
			/* Calculate Total Points */
			var totalPoints = event.roundOneScore + event.roundTwoScore +
						  event.roundThreeScore + event.roundFourScore +
						  event.extraPoints;
			if(contestant.grand_entry_1) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_2) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_3) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_4) {
				totalPoints += 3;
			}
			
			o.contestants.push({contestant: contestant, event: event, totalPoints: totalPoints});
		});
	};
	
	/* get All contestants without the events*/
	o.getAll = function() {
		return $http.get('/contestants').success(function(data){
			angular.copy(data, o.contestants);
		});
	};
	
	o.getByCategory = function(category) {
		return $http.get('/category/' + category)
		.success(function(events){
			while(o.contestants.length > 0) {
				o.contestants.pop();
			}
			for(var i = 0; i < events.length; i++) {
				o.getContestantWithEvent(events[i]);
			}
		});
	};
	
	o.create = function(contestant, events) {
		return $http.post('/contestants', contestant)// tells server to add contestant
		.success(function(data){ // allows us to data bind
			if(data == 97) {
				// this means that there was a duplicate id
				alert("DANCER NOT ADDED: ID is already being used");
				return;
			}
							
			for(var i = 0; i < events.length; i++) {
				o.addEvent({
					category: events[i]
				}, data);
			};		
			
			alert("Successfully Added Dancer");
			return;
		});
	};
	
	o.addEvent = function(event, contestant) {
		return $http.post('/contestants/' + contestant._id + '/events', event)
		.success(function(data){
		});
		
	};
	
	o.remove = function(contestant) {
		return $http.post('/contestants/' + contestant._id + '/remove')
		.success(function(data){
			o.getAll();
		});
	};
	
	o.removeEvent = function(eventId) {
		return $http.post('/events/' + eventId+ '/remove')
		.success(function(removedEvent){});
	};
	
	o.updateScore = function(tableRowObject, option, score) {
		
		if(isNaN(score) || score == '') {
			return;
		}
		
		return $http.put('/events/' + tableRowObject.event._id + '/' + option + '/' + score)
		.success(function(data){
			switch(option) {
				case '1':
					tableRowObject.contestant.roundOneScore = data.roundOneScore;
					break;
				case '2':
					tableRowObject.contestant.roundTwoScore = data.roundTwoScore;
					break;
				case '3':
					tableRowObject.contestant.roundThreeScore = data.roundThreeScore;
					break;
				case '4':
					tableRowObject.contestant.roundFourScore = data.roundFourScore;
					break;
				case '5':
					tableRowObject.contestant.extraPoints = data.extraPoints;
					break;
				default:
					break;
			}
			
			/* Update Total Points */
			var contestant = tableRowObject.contestant;
			var event = data;
			var totalPoints = 0;
		
			totalPoints += event.roundOneScore + event.roundTwoScore +
							  event.roundThreeScore + event.roundFourScore +
							  event.extraPoints;
			if(contestant.grand_entry_1) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_2) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_3) {
				totalPoints += 3;
			}
			if(contestant.grand_entry_4) {
				totalPoints += 3;
			}
		
			tableRowObject.totalPoints = totalPoints;
		});
	};
	
	o.upvote = function(contestant) {
		return $http.put('/contestants/' + contestant._id + '/upvote')
		.success(function(data){
			contestant.upvotes += 1;
		});
	};
	
	o.addComment = function(id, comment) {
		return $http.post('/contestants/' + id + '/comments', comment);
	};
	
	o.upvoteComment = function(contestant, comment){
		return $http.put('/contestants/' + contestant._id + '/comments/' + comment._id + '/upvote')
		.success(function(data){
			comment.upvotes += 1;
		});
	};
	
	/* Grand Entry RESTful Services*/
	o.updateGrandEntry = function (_id, round) {
		return $http.put('/contestants/' + _id + '/grandentry/' + round).then(function(res) {
			console.log(res);
		}).catch(function() {
		});
	};
	
	
	/* Category RESTful Services */
	o.getCategories = function() {
		// Test data
		var newCat = [
			'Men\'s Golden Age',
			'Men\'s Traditional',
			'Men\'s Fancy',
			'Men\'s Grass',
			'Teen Boys\' Traditional',
			'Teen Boys\' Fancy',
			'Teen Boys\' Grass',
			'Junior Boys\' Traditional',
			'Junior Boys\' Fancy',
			'Junior Boys\' Grass',
			'Women\'s Golden Age',
			'Women\'s Traditional',
			'Women\'s Fancy',
			'Women\'s Jingle',
			'Teen Girls\' Traditional',
			'Teen Girls\' Fancy',
			'Teen Girls\' Jingle',
			'Junior Girls\' Traditional',
			'Junior Girls\' Fancy',
			'Junior Girls\' Jingle',
			'Drum'
		];
		o.categories = newCat;
		return;
	};
	
	return o;
}])