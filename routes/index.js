var express = require('express');
var router = express.Router();

/* Log in */
var basicAuth = require('basic-auth-connect');
var auth = basicAuth(function(user, pass){
	return (user === 'superuser' && pass == 'asdfjkl;');
});

var adminAuth = basicAuth(function(user, pass) {
	return (user === 'admin'  && pass == 'admin');
})

/* GET home page. */
router.get('/', auth, function(req, res, next) {
	res.render('index', { title: 'Express' });
});

/* Define variables */
var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var Contestant = mongoose.model('Contestant');
var Event = mongoose.model('Event');

/* GET Operations */
router.get('/categories', function(req, res, next) {
	Category.find(function(err, category) {
		if(err) { return next(err); }
		res.json(category);
	});
});

/* POST Operations */
router.post('/categories', function(req, res, next){
	var category = new Category(req.body);
	category.save(function(err, category){
		if(err){ return next(err); }
		res.json(category);
	});
});

/* POST delete Operations */
router.post('/categories/:categoryId/delete', function(req, res, next) {
	console.log(req.category);
	req.category.remove(function(err, category){
		if(err) { return next(err); }
		res.json(category);
	});
});

/* PUT Operations */


/* Pre-load Operations */
router.param('categoryId', function(req, res, next, id){
	var query = Category.findById(id);
	query.exec(function(err, category){
		if(err){ return next(err); }
		if(!category){ return next(new Error("can't find category")); }
		req.category = category;
		return next();
	});
});





/* PARAM, Pre-load a contestant object */  // this is called before any other contestant route handler
router.param('contestant', function(req, res, next, id){
	var query = Contestant.findById(id);
	
	query.exec(function(err, contestant){
		if(err){ return next(err); }
		if(!contestant){ return next(new Error("can't find contestant")); }
		
		req.contestant = contestant;
		//console.log("PRELOADED CONTESTANT");
		return next();
	});
});

/* GET, get all contestants */ // this is called a request handler
router.get('/contestants', function(req, res, next) {
	Contestant.find(function(err, contestants) {
		if(err) {	return next(err); }
		
		res.json(contestants);
	});
});

/* POST, add a new contestant */
router.post('/contestants', function(req, res, next){
	var contestant = new Contestant(req.body);
	
	contestant.save(function(err, contestant){
		if(err){
			if(err.code == 11000) {
				contestant = 97;
			}
			else {
				return next(err);
			}
		}
		
		res.json(contestant);
	});
});

/* GET, get a single contestant */
router.get('/contestants/:contestant', function(req, res){
	req.contestant.populate('comments', function(err, contestant){
		res.json(contestant);	
	});
});

/* POST, delete a contestant */
router.post('/contestants/:contestant/remove', adminAuth, function(req, res, next) {
	req.contestant.remove(function(err, contestant){
		if(err) { return next(err); }
		
		res.json(contestant);
	});
});

/* POST, add a new event */
router.post('/contestants/:contestant/events', function(req, res, next){
	var event = new Event(req.body);
	event.contestant = req.contestant;
	console.log(event); 
	
	event.save(function(err, contestant){
	if(err){ return next(err); }
		req.contestant.events.push(event);
		
		req.contestant.save(function(err, contestant){
		if(err){ return next(err); }
		
			res.json(event);
		});
	});
});

/* PUT, update Grand Entry */
router.put('/contestants/:contestant/grandentry/:geround', function(req, res, next) {
	req.contestant.updateGrandEntry(req.geRound, function(err, contestant){
		if(err) { return next(err); }
		res.json(contestant);
	});
});

router.param('geround', function(req, res, next, geRound){
	if(geRound === '1' ||
		geRound === '2' ||
		geRound === '3' ||
		geRound === '4') {
			req.geRound = geRound;
			return next();
		}
	else {
		return next(new Error('Grand Entry Round needs to be: [1-4]'));
	}
});



/* PARAM, Pre-load an event object */
router.param('event', function(req, res, next, id){
	var query = Event.findById(id);
	
	query.exec(function(err, event){
		if(err){ return next(err); }
		if(!event){ return next(new Error("can't find event")); }
		
		req.event = event;
		return next();
	});
});

/* GET, get all events */
router.get('/events', function(req, res, next) {
	Event.find(function(err, event){
		if(err) { return next(err); }
		
		res.json(event);
	});
});

/* GET, get a single event */
router.get('/events/:event', function(req, res){
	res.json(req.event);	
});

/* PARAM, pre-load category */
router.param('category', function(req, res, next, newCategory){
	req.category = newCategory;
	return next();
});

/* GET, get all events that belong to Men's Golden Age */
router.get('/category/:category', function(req, res, next) {

	Event.find( {category: req.category} , function(err, events){
		if(err) {
			return next(err); 
		}
		res.json(events);
	});
});

/* POST, remove an event */
router.post('/events/:event/remove', function(req, res, next) {
	req.event.remove(function(err, event){
		if(err) { return next(err); }
		
		res.json(event);
	});
});

/* PUT, score event given a round */
router.put('/events/:event/:option/:score', function(req, res, next) {
	req.event.score(req.round, req.score, function(err, event){
		if(err) { return next(err); }
		
		res.json(event);
	});
});

router.param('option', function(req, res, next, optionNumber){

	if(optionNumber == '1' ||
		optionNumber == '2' ||
		optionNumber == '3' ||
		optionNumber == '4' ||
		optionNumber == '5')
		{
			req.round = optionNumber;
			return next();
		}
	else {
		return next(new Error('The round number is not valid, it needs to be a number 1-5'));
	}
});

router.param('score', function(req, res, next, points){
	if(isNaN(points)){
		return next(new Error('The score is not valid, it needs to be a number'));
	}
	else {
		req.score = points;
		return next();
	}
});


module.exports = router;













